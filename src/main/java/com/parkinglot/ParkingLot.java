package com.parkinglot;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {
    private Car car;
    private int capacity = 10;
    private Map<Ticket, Car> carTicketMap;

    public ParkingLot() {
        carTicketMap = new HashMap<>();
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    public Ticket park(Car car) {
        if(isFull()) {
            throw new NoAvailablePositionException();
        }
        this.car = car;
        Ticket ticket = new Ticket();
        carTicketMap.put(ticket, this.car);
        return ticket;
    }

    public Car fetch(Ticket ticket) {
        if (!carTicketMap.containsKey(ticket) || !isIn(ticket)) {
            throw new UnrecognizedTicketException();
        }
        Car returnCar = carTicketMap.get(ticket);
        carTicketMap.remove(ticket);
        return returnCar;
    }
    public boolean isFull(){
        return carTicketMap.size() == capacity;
    }
    public boolean isIn(Ticket ticket){
       return carTicketMap.containsKey(ticket);
    }

    public int getCount() {
        return carTicketMap.size();
    }
}
