package com.parkinglot;

import java.util.List;

public class ParkingBoy {
    private  ParkingLot parkingLot;
    private  List<ParkingLot> parkingLots;

    public ParkingBoy(ParkingLot parkingLot) {
        this.parkingLot = parkingLot;
    }

    public ParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    public Ticket park(Car car) {
       return parkingLots.stream()
                .filter(lot -> !lot.isFull())
                .findFirst()
                .map(lot -> lot.park(car))
                .orElseThrow(() -> new NoAvailablePositionException());
    }

    public Car fetch(Ticket ticket) {
        return parkingLots.stream()
                .filter(lot->lot.isIn(ticket))
                .findFirst()
                .map(lot->lot.fetch(ticket))
                .orElseThrow(()->new UnrecognizedTicketException());
    }
}
