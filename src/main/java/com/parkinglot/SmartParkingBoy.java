package com.parkinglot;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class SmartParkingBoy {
    private List<ParkingLot> parkingLots;

    public SmartParkingBoy(List<ParkingLot> parkingLosts) {
        this.parkingLots = parkingLosts;
    }

    public Ticket park(Car car) {
//        return parkingLots.stream()
//                .min(Comparator.comparing(ParkingLot::getCount))
//                .map(lot -> lot.park(car));

        ParkingLot minLot = parkingLots.get(0);
        for (ParkingLot parkingLot : parkingLots) {
            if (parkingLot.getCount() < minLot.getCount())
                minLot = parkingLot;
        }
        return minLot.park(car);

    }

    public Car fetch(Ticket ticket) {
        for (ParkingLot parkingLot : parkingLots) {
            if(parkingLot.isIn(ticket)){
                return parkingLot.fetch(ticket);
            }
        }

        throw new UnrecognizedTicketException();
    }
}
