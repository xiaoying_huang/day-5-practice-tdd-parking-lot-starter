package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class SuperSmartParkingBoy {
    private List<ParkingLot> parkingLots = new ArrayList<>();

    public SuperSmartParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;

    }

    public Ticket park(Car car) {
        float availablePositionRate = GetAvailablePositionRate(parkingLots.get(0));
        ParkingLot returnLot = parkingLots.get(0);

        for (ParkingLot parkingLot : parkingLots) {
            if(availablePositionRate < GetAvailablePositionRate(parkingLot)){
                availablePositionRate = GetAvailablePositionRate(parkingLot);
                returnLot = parkingLot;
            }
        }

        return returnLot.park(car);
    }
    private float GetAvailablePositionRate(ParkingLot parkingLot){
       return (parkingLot.getCapacity() - parkingLot.getCount()) / parkingLot.getCapacity();
    }

    public Car fetch(Ticket ticket) {
        for (ParkingLot parkingLot : parkingLots) {
            if(parkingLot.isIn(ticket)){
                return parkingLot.fetch(ticket);
            }
        }
        throw new UnrecognizedTicketException();
    }
}
