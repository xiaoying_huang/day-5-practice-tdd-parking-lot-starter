package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {
    @Test
    //Story1-Case 1
    void should_return_a_ticket_when_park_car_given_a_car_and_parking_lot() {
        //given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();

        //when
        Ticket ticket = parkingLot.park(car);

        //then
        Assertions.assertNotNull(ticket);
    }

    @Test
    //Story1-Case 2
    void should_return_a_car_when_fetch_a_car_given_a_ticket_and_a_parking_lot_parked_car() {
        //given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        Ticket ticket = parkingLot.park(car);

        //when
        Car fetchedCar = parkingLot.fetch(ticket);

        //then
        assertEquals(car, fetchedCar);
    }

    @Test
    //Story1-Case 3
    void should_return_right_car_when_park_car_given_two_tickets_and_parking_lot_with_two_parked_cars() {
        //given
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot();
        Ticket ticket1 = parkingLot.park(car1);
        Ticket ticket2 = parkingLot.park(car2);

        //when
        Car fetchedCar1 = parkingLot.fetch(ticket1);
        Car fetchedCar2 = parkingLot.fetch(ticket2);

        //then
        assertEquals(car1, fetchedCar1);
        assertEquals(car2, fetchedCar2);
    }

    @Test
    //Story1-Case4-Add Story2-Case2-Modify
    void should_return_nothing_with_message_when_park_given_a_wrong_ticket_and_a_parking_lot() {
        //given
        ParkingLot parkingLot = new ParkingLot();

        //when then
        UnrecognizedTicketException unrecognizedTicketException =
                assertThrows(UnrecognizedTicketException.class,
                () -> parkingLot.fetch(new Ticket()));
        assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    //Story1-Case5-Add Story2-Case2-Modify
    void should_return_nothing_with_message_when_fetch_car_given_a_used_ticket_and_a_parking_lot() {
        //given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        Ticket ticket = parkingLot.park(car);
        Car fetchedCar1 = parkingLot.fetch(ticket);

        //when then
        UnrecognizedTicketException unrecognizedTicketException =
                assertThrows(UnrecognizedTicketException.class,
                        () -> parkingLot.fetch(ticket));
        assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    //Story1-Case6-Add Story2-Case3-Modify
    void should_return_nothing_with_message_when_park_given_a_parking_lot_without_any_position() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        for (int i = 0; i < 10; i++) {
            parkingLot.park(new Car());
        }

        //when then
        NoAvailablePositionException noAvailablePositionException =
                assertThrows(NoAvailablePositionException.class, ()-> parkingLot.park(new Car()));
        assertEquals("No available position.", noAvailablePositionException.getMessage());
    }
}
