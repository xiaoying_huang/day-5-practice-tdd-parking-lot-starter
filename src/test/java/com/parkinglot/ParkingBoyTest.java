package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ParkingBoyTest {
    @Test
    //Story3-Case1-Add Story4-Case1-Modify
    void should_return_ticket_in_the_first_lot_when_park_a_car_given_a_car_and_two_parking_lots_and_a_parking_boy() {
        //given
        Car car = new Car();
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);

        //when
        Ticket ticket = parkingBoy.park(car);

        //then
        Assertions.assertTrue(parkingLot1.isIn(ticket));
        Assertions.assertFalse(parkingLot2.isIn(ticket));
    }
    @Test
    //Story4-Case2-Add
    void should_return_ticket_in_the_second_lot_when_park_given_a_car_and_one_full_parkingLots_another_enough_and_parking_boy() {
        //given
        Car car = new Car();
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        for (int i = 0; i < 10; i++) {
            parkingLot1.park(new Car());
        }

        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);

        //when
        Ticket ticket = parkingBoy.park(car);

        //then
        Assertions.assertFalse(parkingLot1.isIn(ticket));
        Assertions.assertTrue(parkingLot2.isIn(ticket));
    }
    @Test
    //Story3-Case2-Add Modify
    void should_return_a_parked_car_when_fetch_given_a_ticket_and_a_parking_lot_and_a_parking_boy() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot = new ParkingLot();
        parkingLots.add(parkingLot);

        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);

        Car car = new Car();

        Ticket ticket = parkingBoy.park(car);

        //when
        Car fetchedCar = parkingBoy.fetch(ticket);

        //then
        assertEquals(car, fetchedCar);
    }

    @Test
    //Story3-Case3-Add Story4-Case3-Modify
    void  should_fetch_right_two_car_when_fetch_twice_given_two_parkingLots_both_with_a_parked_car_and_two_tickets() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);

        Car car1 = new Car();
        Car car2 = new Car();

        Ticket ticket1 = parkingBoy.park(car1);
        Ticket ticket2 = parkingBoy.park(car2);

        //when then
        Car fetchedCar1 = parkingBoy.fetch(ticket1);
        assertEquals(car1, fetchedCar1);

        Car fetchedCar2 = parkingBoy.fetch(ticket2);
        assertEquals(car2, fetchedCar2);

    }

    @Test
    //Story4-Case4
    void should_return_with_message_when_fetch_given_wrong_ticket_and_two_parkingLots() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);

        //when then
        UnrecognizedTicketException unrecognizedTicketException =
                assertThrows(UnrecognizedTicketException.class,
                        () -> parkingBoy.fetch(new Ticket()));
        assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());

    }

    @Test
    //Story4-Case5
    void should_return_message_when_fetch_given_two_parkingLots_and_a_used_ticket() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);

        Car car = new Car();

        Ticket ticket = parkingBoy.park(car);
        Car fetchedCar = parkingBoy.fetch(ticket);

        //when then
        UnrecognizedTicketException unrecognizedTicketException =
                assertThrows(UnrecognizedTicketException.class,
                        () -> parkingBoy.fetch(ticket));
        assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    //Story4-Case6
    void should_return_NoAvailablePosition_message_when_park_given_two_parkingLots_both_without_position_and_a_car() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);

        for (int i = 0; i < 10; i++) {
            parkingLot1.park(new Car());
            parkingLot2.park(new Car());
        }

        //when then
        NoAvailablePositionException noAvailablePositionException =
                assertThrows(NoAvailablePositionException.class, ()-> parkingBoy.park(new Car()));
        assertEquals("No available position.", noAvailablePositionException.getMessage());

    }
}
