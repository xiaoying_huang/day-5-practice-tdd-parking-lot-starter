package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class SuperSmartParkingBoyTest {
    //set the capacity of lot1 is 10, lot2 is 20
    @Test
    //Story6-Case1
    void should_return_car_in_the_first_lot_when_park_given_two_empty_parkinglots_and_a_car() {
        //given
        Car car = new Car();
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot();
        parkingLot1.setCapacity(10);
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot2.setCapacity(20);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);

        //when
        Ticket ticket = superSmartParkingBoy.park(car);

        //then
        Assertions.assertTrue(parkingLot1.isIn(ticket));
        Assertions.assertFalse(parkingLot2.isIn(ticket));
    }

    @Test
    //Story6-Case2
    //lot1_AvailablePositionRate = 4 / 10 = 0.4, lot2_AvailablePositionRate = 6 / 20 = 3 / 10 = 0.3
    //lot1 = 6 / 10 = 0.6, lot2 = 4 / 20 = 2 / 10 = 0.2
    void should_return_park_in_the_first_lot_when_park_given_two_parkingLots_with_4_6_cars() {
        //given
        Car car = new Car();
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot1.setCapacity(10);
        parkingLot2.setCapacity(20);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);

        for (int i = 0; i < 4; i++) {
            parkingLot1.park(new Car());
            parkingLot2.park(new Car());
        }
        for (int i = 0; i < 2; i++) {
            parkingLot2.park(new Car());
        }

        Car car1 = new Car();

        //when
        Ticket ticket = superSmartParkingBoy.park(car1);

        //then
        assertTrue(parkingLot1.isIn(ticket));
    }

    @Test
    //Story6-Case3
    void should_fetch_right_two_cars_when_fetch_given_two_parkingLots_both_with_a_parked_car_and_two_tickets() {
        //given
        Car car = new Car();
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot();
        parkingLot1.setCapacity(10);
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot2.setCapacity(20);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);

        Car car1 = new Car();
        Car car2 = new Car();

        Ticket ticket1 = superSmartParkingBoy.park(car1);
        Ticket ticket2 = superSmartParkingBoy.park(car2);

        //when then
        Car fetchedCar1 = superSmartParkingBoy.fetch(ticket1);
        assertEquals(car1, fetchedCar1);

        Car fetchedCar2 = superSmartParkingBoy.fetch(ticket2);
        assertEquals(car2, fetchedCar2);
    }

    @Test
    //Story5-Case4
    void should_return_Unrecognized_Parking_ticket_message_when_fetch_given_a_wrong_ticket() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot1.setCapacity(10);
        parkingLot2.setCapacity(20);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);

        //when then
        UnrecognizedTicketException unrecognizedTicketException =
                assertThrows(UnrecognizedTicketException.class,
                        () -> superSmartParkingBoy.fetch(new Ticket()));
        assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    //Story6-Case5
    void should_return_Unrecognized_Parking_ticket_message_when_fetch_given_a_used_ticket() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot1.setCapacity(10);
        parkingLot2.setCapacity(20);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);

        Car car = new Car();
        Ticket ticket = superSmartParkingBoy.park(car);
        superSmartParkingBoy.fetch(ticket);

        //when then
        UnrecognizedTicketException unrecognizedTicketException =
                assertThrows(UnrecognizedTicketException.class,
                        () -> superSmartParkingBoy.fetch(ticket));
        assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    //Story6-Case6
    void should_return_NoAvailablePosition_message_when_park_given_two_parkingLots_both_without_position_and_a_car() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot1.setCapacity(10);
        parkingLot2.setCapacity(20);
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);

        for (int i = 0; i < 10; i++) {
            parkingLot1.park(new Car());
            parkingLot2.park(new Car());
        }
        for (int i = 0; i <10; i++) {
            parkingLot2.park(new Car());
        }

        //when
        //when then
        NoAvailablePositionException noAvailablePositionException =
                assertThrows(NoAvailablePositionException.class, ()-> superSmartParkingBoy.park(new Car()));
        assertEquals("No available position.", noAvailablePositionException.getMessage());
    }
}
