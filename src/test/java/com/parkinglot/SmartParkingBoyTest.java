package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import static org.junit.jupiter.api.Assertions.*;

//SmartParkingBoy:park car to the parkingLot which contains more empty positions
public class SmartParkingBoyTest {
    @Test
    //Story5-Case1
    void should_return_car_in_the_first_lot_when_park_given_two_empty_parkinglots_and_a_car() {
        //given
        Car car = new Car();
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);

        //when
        Ticket ticket = smartParkingBoy.park(car);

        //then
        Assertions.assertTrue(parkingLot1.isIn(ticket));
        Assertions.assertFalse(parkingLot2.isIn(ticket));

    }

    @Test
    //Story5-Case2
    void should_return_park_in_the_first_lot_when_park_given_two_parkingLots_with_4_6_cars() {
        //given
        Car car = new Car();
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);

        for (int i = 0; i < 4; i++) {
            parkingLot1.park(new Car());
            parkingLot2.park(new Car());
        }
        for (int i = 0; i < 2; i++) {
            parkingLot2.park(new Car());
        }

        Car car1 = new Car();

        //when
        Ticket ticket = smartParkingBoy.park(car1);

        // then
        assertTrue(parkingLot1.isIn(ticket));

    }

    @Test
    //Story5-Case3
    void should_fetch_right_two_cars_when_fetch_given_two_parkingLots_both_with_a_parked_car_and_two_tickets() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);

        Car car1 = new Car();
        Car car2 = new Car();

        Ticket ticket1 = smartParkingBoy.park(car1);
        Ticket ticket2 = smartParkingBoy.park(car2);

        //when then
        Car fetchedCar1 = smartParkingBoy.fetch(ticket1);
        assertEquals(car1, fetchedCar1);

        Car fetchedCar2 = smartParkingBoy.fetch(ticket2);
        assertEquals(car2, fetchedCar2);
    }

    @Test
    //Story5_Case4
    void should_return_Unrecognized_Parking_ticket_message_when_fetch_given_a_wrong_ticket() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);

        //when then
        UnrecognizedTicketException unrecognizedTicketException =
                assertThrows(UnrecognizedTicketException.class,
                        () -> smartParkingBoy.fetch(new Ticket()));
        assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    //Story5-Case5
    void should_return_Unrecognized_Parking_ticket_message_when_fetch_given_a_used_ticket() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);

        Car car = new Car();
        Ticket ticket = smartParkingBoy.park(car);
        smartParkingBoy.fetch(ticket);

        //when then
        UnrecognizedTicketException unrecognizedTicketException =
                assertThrows(UnrecognizedTicketException.class,
                        () -> smartParkingBoy.fetch(ticket));
        assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    //Story5-Case6
    void should_return_NoAvailablePosition_message_when_park_given_two_parkingLots_both_without_position_and_a_car() {
        //given
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);

        for (int i = 0; i < 10; i++) {
            parkingLot1.park(new Car());
            parkingLot2.park(new Car());
        }

        //when
        //when then
        NoAvailablePositionException noAvailablePositionException =
                assertThrows(NoAvailablePositionException.class, ()-> smartParkingBoy.park(new Car()));
        assertEquals("No available position.", noAvailablePositionException.getMessage());

    }
}
